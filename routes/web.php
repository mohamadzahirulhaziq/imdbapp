<?php

use App\Http\Controllers\ActorController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\ProducerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", [MovieController::class, "index"])->name("movie.index");
Route::get("movie-add", [MovieController::class, "add"])->name("movie.add");
Route::post("actor-save", [ActorController::class, "save"])->name("actor.save");
Route::post("producer-save", [ProducerController::class, "save"])->name("producer.save");
Route::post("movie-save", [MovieController::class, "save"])->name("movie.save");
Route::get("movie-edit/{id}", [MovieController::class, "edit"])->name("movie.edit");
Route::get("movie-detail/{id}", [MovieController::class, "detail"])->name("movie.detail");

Route::get("movie-deleteMovie/{id}", [MovieController::class, "deleteMovie"])->name("movie.deleteMovie");

Route::post("movie-update", [MovieController::class, "updateMovie"])->name("movie.updateMovie");


