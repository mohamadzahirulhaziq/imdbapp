<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use Illuminate\Http\Request;

class ActorController extends Controller
{
    public function save(Request $request)
	{
        $actor = new Actor();
        $actor->name = $request->name;
        $actor->sex = $request->sex;
        $actor->dob = $request->dob;
        $actor->bio = $request->bio;
        $actor->save();
    }
}
