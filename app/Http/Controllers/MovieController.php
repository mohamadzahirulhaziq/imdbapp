<?php

namespace App\Http\Controllers;

use App\Models\Actor;
use App\Models\Movie;
use App\Models\Producer;
use Illuminate\Http\Request;
use Inertia\Inertia;

class MovieController extends Controller
{
    public function index(){

        $movies = Movie::with('actors','producer')->get();
        return Inertia::render("Welcome", [
			"movies" => $movies,
		]);
    }

    public function add(){

        $actors = Actor::all();
        $producers = Producer::all();
        return Inertia::render("AddMovie", [
			"movies" => 'xx',
			"actors" => $actors,
			"producers" => $producers,
		]);
    }

    public function save(Request $request){

        $actorsIds = [];
        foreach($request->actorsValue as $actorIds){
            $actorsIds[] = $actorIds['id'];
        }

        
        $movie = new Movie();
        $movie->name = $request->title;
        $movie->year = $request->dateOfRelease;
        $movie->plot = 'test';
        $movie->poster = 'test';
        $movie->pro_id = $request->producer['id'];
        $movie->save();

        $actor = Actor::find($actorsIds);
        $movie->actors()->attach($actor);

        return redirect()->back();

    }

    public function edit($id){

        $movie = Movie::with('actors','producer')->whereId($id)->first();//dd($movie);
        $actors = Actor::all();
        $producers = Producer::all();
        return Inertia::render("EditMovie", [
			"movies" => $movie,
            "actors" => $actors,
			"producers" => $producers,
		]);

    }

    public function detail($id){

        $movie = Movie::with('actors','producer')->whereId($id)->first();//dd($movie);
        $actors = Actor::all();
        $producers = Producer::all();
        return Inertia::render("DetailMovie", [
			"movies" => $movie,
            "actors" => $actors,
			"producers" => $producers,
		]);

    }

    public function updateMovie(Request $request){
        
        $actorsIds = [];
        foreach($request->actorsValue as $actorIds){
            $actorsIds[] = $actorIds['id'];
        }

        
        $movie = Movie::whereId($request->movieId)->first();
        $movie->name = $request->title;
        $movie->year = $request->dateOfRelease;
        $movie->plot = 'test';
        $movie->poster = 'test';
        $movie->pro_id = $request->producer['id'];
        $movie->save();

        $actor = Actor::find($actorsIds);
        $movie->actors()->sync($actor);

        return redirect()->back();
    }

    public function deleteMovie($id, Request $request)
	{
        $movie = Movie::where('id',$id)->first();

        $movie->delete();
        return redirect()->back();
    }

}
