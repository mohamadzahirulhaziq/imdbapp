<?php

namespace App\Http\Controllers;

use App\Models\Producer;
use Illuminate\Http\Request;

class ProducerController extends Controller
{
    public function save(Request $request)
	{
        $producer = new Producer();
        $producer->name = $request->name;
        $producer->sex = $request->sex;
        $producer->dob = $request->dob;
        $producer->bio = $request->bio;
        $producer->save();
    }
}
