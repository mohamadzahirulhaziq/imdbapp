import Vue from 'vue'
import { createInertiaApp } from '@inertiajs/inertia-vue'
import { InertiaProgress } from '@inertiajs/progress'
import flatPickr from "vue-flatpickr-component";

InertiaProgress.init()

createInertiaApp({
  resolve: name => require(`./Pages/${name}`),
  setup({ el, App, props, plugin }) {
    Vue.use(plugin)
    Vue.use(flatPickr);

    new Vue({
      render: h => h(App, props),
    }).$mount(el)
  },
})
